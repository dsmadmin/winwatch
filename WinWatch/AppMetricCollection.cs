﻿/*
 * This file is part of WinWatch 
 * 
 * WinWatch - Monitoring Windows in the Cloud
    Copyright (C) 2011  Enhance Web Ltd

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
 *  <https://bitbucket.org/EnhanceChris/winwatch/wiki/License>.
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;

using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;

namespace Ec2Tools.WinWatch
{
    public class AppMetricCollection : List<AppMetric>
    {
        private AppMetricCollection(Metrics configMetrics)
        {
            foreach (Metric metric in configMetrics)
            {
                try
                {
                    AppMetric appMetric = AppMetric.NewAppMetric(metric);

                    if (appMetric.PerfCounter != null)
                    {
                        this.Add(appMetric);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteExToLog(ex);
                }
            }
        }

        public static AppMetricCollection NewMetricCollection(Metrics configMetrics)
        {
            return new AppMetricCollection(configMetrics);
        }

        public void LogMetrics(WinWatchConfig WinWatchConfig, IList<MetricDatum> data)
        {
            int LastDelayIndex = this.Count - 1;

            for(int i = 0; i < this.Count; i++)
            {
                AppMetric appmetric = this[i];

                try
                {
                    appmetric.LogMetric(WinWatchConfig, data);

                    if (WinWatchConfig.DelayBetweenMetricReading > 0
                        && i < LastDelayIndex)
                    {
                        Logger.WriteToLog("Sleeping for " + WinWatchConfig.DelayBetweenMetricReading +
                            "ms between metrics", LogMessageLevel.Verbose);
                        Thread.Sleep(WinWatchConfig.DelayBetweenMetricReading);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteExToLog(ex);
                }

            }
        }
    }
}

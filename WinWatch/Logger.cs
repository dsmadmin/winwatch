﻿/*
 * This file is part of WinWatch 
 * 
 * WinWatch - Monitoring Windows in the Cloud
    Copyright (C) 2011  Enhance Web Ltd

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
 *  <https://bitbucket.org/EnhanceChris/winwatch/wiki/License>.
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.IO;

namespace Ec2Tools.WinWatch
{
    public static class Logger
    {
        static Logger()
        {
            NameValueCollection appConfig = ConfigurationManager.AppSettings;
            _enablelogging = bool.Parse(appConfig["EnableLogging"]);
            _enableloggingtodisk = bool.Parse(appConfig["EnableLoggingToDisk"]);
            _diskloggingpath = appConfig["DiskLoggingPath"];
            _verboselogging = bool.Parse(appConfig["VerboseLogging"]);
        }

        private static bool _enablelogging;
        private static bool EnableLogging
        {
            get
            {
                return _enablelogging;
            }
        }

        private static bool _enableloggingtodisk;
        private static bool EnableLoggingToDisk
        {
            get
            {
                return _enableloggingtodisk;
            }
        }

        private static string _diskloggingpath;
        private static string DiskLoggingPath
        {
            get
            {
                return _diskloggingpath;
            }
        }

        private static bool _verboselogging;
        private static bool VerboseLogging
        {
            get
            {
                return _verboselogging;
            }
        }

        public static void WriteToLog(string LogMessage)
        {
            WriteToLog(LogMessage, LogMessageLevel.Message);
        }

        public static void WriteToLog(string LogMessage, LogMessageLevel Level)
        {
            if (EnableLogging == true)
            {
                if(Level == LogMessageLevel.Message || VerboseLogging == true)
                Console.WriteLine(LogMessage);

                if (EnableLoggingToDisk == true)
                {
                    using (StreamWriter w = File.AppendText(DiskLoggingPath))
                    {
                        w.WriteLine(LogMessage);

                        w.Close();
                    }
                }
            }
        }

        public static void WriteExToLog(Exception ex)
        {
            string LogMessage = ex.GetType().Name + ":" + ex.Message + Environment.NewLine +
                        ex.StackTrace + Environment.NewLine +
                        ex.Source;

            WriteToLog(LogMessage);
        }
    }

    public enum LogMessageLevel
    {
        Message,
        Verbose
    }
}

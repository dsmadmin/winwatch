﻿/*
 * This file is part of WinWatch 
 * 
 * WinWatch - Monitoring Windows in the Cloud
    Copyright (C) 2011  Enhance Web Ltd

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
 *  <https://bitbucket.org/EnhanceChris/winwatch/wiki/License>.
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;
using System.Threading;
using System.Configuration;
using System.Collections.Specialized;

using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;

namespace Ec2Tools.WinWatch
{
    public class WinWatchTask
    {
        MetricConfigurationSection _config = null;

        protected MetricConfigurationSection Config
        {
            get
            {
                return _config;
            }
        }

        protected void LoadMetricConfig()
        {
            _config =
            (MetricConfigurationSection)System.Configuration.ConfigurationManager.GetSection(
            "MetricConfigurationGroup/MetricConfigurationSection");
        }

        private WinWatchConfig WinWatchConfig
        {
            get;
            set;
        }

        private AppMetricCollection appmetrics
        {
            get;
            set;
        }

        private AmazonCloudWatch client
        {
            get;
            set;
        }

        protected bool LoadConfigs()
        {
            bool StartupSuccess = true;

            try
            {
                WinWatchConfig = new Ec2Tools.WinWatch.WinWatchConfig();
                WinWatchConfig.LoadConfigFromAppSettings();

                LoadMetricConfig();
                appmetrics = AppMetricCollection.NewMetricCollection(Config.Metrics);

                AmazonCloudWatchConfig cwconfig = new AmazonCloudWatchConfig();
                cwconfig.ServiceURL = WinWatchConfig.ServiceURI;

                if (WinWatchConfig.DisableCloudWatchAccess == false)
                {
                    client = Amazon.AWSClientFactory.CreateAmazonCloudWatchClient(
                         WinWatchConfig.accessKeyID, WinWatchConfig.secretAccessKeyID, cwconfig);
                }
            }
            catch (Exception ex)
            {
                StartupSuccess = false;
                Logger.WriteExToLog(ex);
            }

            return StartupSuccess;
        }

        public void DoWinWatch()
        {
            bool StartupSuccess = LoadConfigs();

            DateTime CurrentStartTime = DateTime.Now;
            DateTime LastStartTime = DateTime.Now;

            if (StartupSuccess == true)
            {
                bool terminateLoop = false;
                DateTime DesiredNextLogTime = DateTime.Now;

                while (terminateLoop == false)
                {
                    LastStartTime = CurrentStartTime;
                    CurrentStartTime = DateTime.Now;

                    TimeSpan TimeBetweenLastReading = CurrentStartTime - LastStartTime;
                    Logger.WriteToLog("Time Between Last Reading " + 
                        TimeBetweenLastReading.TotalMilliseconds +
                        "ms", LogMessageLevel.Verbose);

                    IList<MetricDatum> data = new List<MetricDatum>();
                    appmetrics.LogMetrics(WinWatchConfig, data);
                    terminateLoop = SendUpdateToCloudWatch(data);

                    if (terminateLoop == false)
                    {
                        DesiredNextLogTime = DesiredNextLogTime.AddSeconds(
                            WinWatchConfig.LogFrequency);
                        SleepTilNextUpdate(DesiredNextLogTime);
                    }
                }
            }
            else
            {
                Logger.WriteToLog("WinWatch did not startup correctly.");
            }
        }

        protected void SleepTilNextUpdate(DateTime DesiredLogTime)
        {
            Logger.WriteToLog("Desired Log Time " + DesiredLogTime, LogMessageLevel.Verbose);

            DateTime Now = DateTime.Now;
            TimeSpan TimeTillNextLog = DesiredLogTime - Now;
            int SleepMilliSeconds = (int)TimeTillNextLog.TotalMilliseconds;

            Logger.WriteToLog("Sleeping for " + SleepMilliSeconds +
                        "ms between readings", LogMessageLevel.Verbose);

            Thread.Sleep(SleepMilliSeconds);
        }

        protected bool SendUpdateToCloudWatch(IList<MetricDatum> data)
        {
            bool failure = false;

            if (WinWatchConfig.DisableCloudWatchAccess == false)
            {
                try
                {
                    client.PutMetricData(new PutMetricDataRequest()
                            .WithMetricData(data)
                            .WithNamespace(WinWatchConfig.CloudWatchNamespace));
                }
                catch (Exception ex)
                {
                    failure = true;
                    Logger.WriteExToLog(ex);
                }
            }

            return failure;
        }
    }
}

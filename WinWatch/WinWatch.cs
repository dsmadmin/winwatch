﻿/*******************************************************************************
* Copyright 2009-2011 Amazon.com, Inc. or its affiliates. All Rights Reserved.
* 
* Licensed under the Apache License, Version 2.0 (the "License"). You may
* not use this file except in compliance with the License. A copy of the
* License is located at
* 
* http://aws.amazon.com/apache2.0/
* 
* or in the "license" file accompanying this file. This file is
* distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied. See the License for the specific
* language governing permissions and limitations under the License.
*******************************************************************************/

/* The WinWatch project started as a copy of an Amazon Example hence the above notice
 * WinWatch modifications are under GPL */

/*
 * This file is part of WinWatch 
 * 
 * WinWatch - Monitoring Windows in the Cloud
    Copyright (C) 2011  Enhance Web Ltd

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
 *  <https://bitbucket.org/EnhanceChris/winwatch/wiki/License>.
 * */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;
using System.Threading;
using System.Configuration;
using System.Collections.Specialized;

using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;

namespace Ec2Tools.WinWatch
{
    class WinWatch
    {
        static void Main(string[] args)
        {
            OutputConsoleHeader();
            HandleShowLicense(args);

            WinWatchTask winwatchtask = new WinWatchTask();
            winwatchtask.DoWinWatch();


        }

        protected static void HandleShowLicense(string[] args)
        {
            if(args.Count() == 2)
            {
                if (args[0] == "show")
                {
                    if (args[1] == "warranty")
                    {
                        Console.WriteLine("15. Disclaimer of Warranty.");
                        Console.WriteLine("");
                        Console.WriteLine("  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY");
                        Console.WriteLine("APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT");
                        Console.WriteLine("HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM \u0022 AS IS\u0022 WITHOUT WARRANTY");
                        Console.WriteLine("OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,");
                        Console.WriteLine("THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR");
                        Console.WriteLine("PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM");
                        Console.WriteLine("IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF");
                        Console.WriteLine("ALL NECESSARY SERVICING, REPAIR OR CORRECTION.");
                        Console.WriteLine("");
                        Console.WriteLine("  16. Limitation of Liability.");
                        Console.WriteLine("");
                        Console.WriteLine("  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING");
                        Console.WriteLine("WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS");
                        Console.WriteLine("THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY");
                        Console.WriteLine("GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE");
                        Console.WriteLine("USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF");
                        Console.WriteLine("DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD");
                        Console.WriteLine("PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),");
                        Console.WriteLine("EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF");
                        Console.WriteLine("SUCH DAMAGES.");
                        Console.WriteLine("");
                        Console.WriteLine("  17. Interpretation of Sections 15 and 16.");
                        Console.WriteLine("");
                        Console.WriteLine("  If the disclaimer of warranty and limitation of liability provided");
                        Console.WriteLine("above cannot be given local legal effect according to their terms,");
                        Console.WriteLine("reviewing courts shall apply local law that most closely approximates");
                        Console.WriteLine("an absolute waiver of all civil liability in connection with the");
                        Console.WriteLine("Program, unless a warranty or assumption of liability accompanies a");
                        Console.WriteLine("copy of the Program in return for a fee.");
                    }
                    else if (args[1] == "copyright")
                    {
                        OutputConsoleHeader();
                    }
                }
            }
        }

        protected static void OutputConsoleHeader()
        {
            Console.WriteLine("WinWatch  Copyright (C) 2011 Enhance Web Ltd");
            Console.WriteLine("This program comes with ABSOLUTELY NO WARRANTY; for details type `WinWatch.exe show warranty'.");
            Console.WriteLine("This is free software, and you are welcome to redistribute it");
            Console.WriteLine("under certain conditions; type `WinWatch.exe show copyright' for details.");
        }
    }

    
}

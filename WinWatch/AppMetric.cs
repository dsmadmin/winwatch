﻿/*
 * This file is part of WinWatch 
 * 
 * WinWatch - Monitoring Windows in the Cloud
    Copyright (C) 2011  Enhance Web Ltd

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
 *  <https://bitbucket.org/EnhanceChris/winwatch/wiki/License>.
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;

namespace Ec2Tools.WinWatch
{
    public class AppMetric
    {
        protected Metric appconfigMetric = null;

        private AppMetric(Metric configMetric)
        {
            appconfigMetric = configMetric;

            _perfcounter = InitPerfCounter(configMetric);
            _metricname = configMetric.MetricName.Replace(" ", "");
            _unit = configMetric.Unit;
        }

        protected PerformanceCounter InitPerfCounter(Metric configMetric)
        {
            PerformanceCounter perfCounter = null;

            if (string.IsNullOrEmpty(configMetric.InstanceName) == false)
            {
                try
                {
                    perfCounter =
                    new PerformanceCounter(configMetric.CategoryName, configMetric.CounterName,
                        configMetric.InstanceName);
                }
                catch (InvalidOperationException invalidopex)
                {
                    Logger.WriteExToLog(invalidopex);
                    perfCounter = null;
                }
            }
            else
            {
                try
                {
                    perfCounter =
                        new PerformanceCounter(
                            configMetric.CategoryName, configMetric.CounterName);
                }
                catch (InvalidOperationException invalidopex)
                {
                    Logger.WriteExToLog(invalidopex);
                    perfCounter = null;
                }
            }

            return perfCounter;
        }

        public static AppMetric NewAppMetric(Metric configMetric)
        {
            return new AppMetric(configMetric);
        }

        private PerformanceCounter _perfcounter = null;
        public PerformanceCounter PerfCounter
        {
            get
            {
                return _perfcounter;
            }
        }

        private string _metricname;
        public string MetricName
        {
            get
            {
                return _metricname;
            }
        }

        private string _unit;
        public string Unit
        {
            get
            {
                return _unit;
            }
        }

        public void LogMetric(WinWatchConfig WinWatchConfig, IList<MetricDatum> data)
        {
            if (this.PerfCounter != null)
            {
                float metricValue = this.PerfCounter.NextValue();

                if (WinWatchConfig.DisableCloudWatchAccess == false)
                {
                    data.Add(new MetricDatum()
                        .WithMetricName(WinWatchConfig.metricPrefix +
                        this.MetricName)
                        .WithTimestamp(DateTime.Now)
                        .WithUnit(this.Unit)
                        .WithValue(metricValue));

                }

                Logger.WriteToLog(WinWatchConfig.metricPrefix +
                    this.MetricName + " : " +
                    DateTime.Now + " : " + this.Unit + " : " +
                    " : " + metricValue);
            }
        }
    }
}

﻿WinWatch
WinWatch is an application developed us, Enhance Web Ltd, to help monitor 
our Amazon EC2 Cloud Servers. We are releasing it as an Open Source 
Application Under the GPL to help other Amazon Cloud Windows users 
get quickly started with monitoring their servers.

It was originally based on an example from the Amazon SDK which provided 
a similar service but would of required a developer to recompile the app 
every time the monitors need to be changed.

Overview
The application is configured from within the App.Config file. You configure 
any number of Windows Performance Counters to check and WinWatch 
periodically pipes these through to CloudWatch. The period is definable 
in seconds. The App.Config is pretty self explanatory and starts with 
an example setup.

Usage
The application is designed to be run as a scheduled task. If for any 
reason it fails the task scheduler should re-launch it at an appropriate 
time. If you intend to or do use the program within your cloud deployment 
we would love to hear from you, please email c.hunt@enhancecms.com.

Please make sure you read and agree to the License before using WinWatch.

Helping
You want to help develop WinWatch further? Great! Please 
contact c.hunt@enhancecms.com to request access. 
You can also checkout our project at 
https://bitbucket.org/EnhanceChris/winwatch/wiki/RoadMap
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;

namespace Ec2Tools.WinWatch
{
    public class WinWatchConfig
    {
        public string accessKeyID
        {
            get;
            set;
        }

        public string secretAccessKeyID
        {
            get;
            set;
        }

        public string metricPrefix
        {
            get;
            set;
        }

        public string CloudWatchNamespace
        {
            get;
            set;
        }

        public string ServiceURI
        {
            get;
            set;
        }

        public int LogFrequency
        {
            get;
            set;
        }

        public bool DisableCloudWatchAccess
        {
            get;
            set;
        }

        public int DelayBetweenMetricReading
        {
            get;
            set;
        }


        public void LoadConfigFromAppSettings()
        {
            NameValueCollection appConfig = ConfigurationManager.AppSettings;

            accessKeyID = appConfig["AWSAccessKey"];
            secretAccessKeyID = appConfig["AWSSecretKey"];
            metricPrefix = appConfig["MetricPrefix"];
            CloudWatchNamespace = appConfig["CloudWatchNamespace"];
            ServiceURI = appConfig["ServiceURI"];
            LogFrequency = (int)uint.Parse(appConfig["LogFrequency"]);
            DisableCloudWatchAccess = bool.Parse(appConfig["DisableCloudWatchAccess"]);
            DelayBetweenMetricReading = (int)uint.Parse(appConfig["DelayBetweenMetricReading"]);
        }
    }
}

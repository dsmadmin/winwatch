﻿/*
 * WinWatch - Monitoring Windows in the Cloud
    Copyright (C) 2011  Enhance Web Ltd

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
 *  <https://bitbucket.org/EnhanceChris/winwatch/wiki/License>.
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Ec2Tools.WinWatch
{
    public class MetricConfigurationSection : System.Configuration.ConfigurationSection
    {
        [ConfigurationProperty("Metrics")]
        public Metrics Metrics
        {
            get
            {
                return (Metrics)base["Metrics"];
            }
            set
            { this["Metrics"] = value; }
        }
    }


    public class Metrics : ConfigurationElementCollection
    {
        protected override
            ConfigurationElement CreateNewElement()
        {
            return new Metric();
        }

        protected override Object
            GetElementKey(ConfigurationElement element)
        {
            return ((Metric)element).MetricName;
        }
    }



    public class Metric : ConfigurationElement
    {
        [ConfigurationProperty("CategoryName", IsRequired = true)]
        public String CategoryName
        {
            get
            {
                return (String)this["CategoryName"];
            }
            set
            {
                this["CategoryName"] = value;
            }
        }

        [ConfigurationProperty("CounterName", IsRequired = true)]
        public String CounterName
        {
            get
            {
                return (String)this["CounterName"];
            }
            set
            {
                this["CounterName"] = value;
            }
        }

        [ConfigurationProperty("InstanceName", IsRequired = false)]
        public String InstanceName
        {
            get
            {
                return (String)this["InstanceName"];
            }
            set
            {
                this["InstanceName"] = value;
            }
        }

        [ConfigurationProperty("MetricName", IsRequired = true)]
        public String MetricName
        {
            get
            {
                return (String)this["MetricName"];
            }
            set
            {
                this["MetricName"] = value;
            }
        }

        [ConfigurationProperty("Unit", IsRequired = true)]
        public String Unit
        {
            get
            {
                return (String)this["Unit"];
            }
            set
            {
                this["Unit"] = value;
            }
        }
    }
}
